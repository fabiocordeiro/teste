/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.gov.tce.testes;

import java.io.File;

/**
 *
 * @author fabio
 */
public class MudaExtensao {

  public static void main(String[] args) {
      renomearExtensaoPraMinusculoDosArquivosDeImportacao("/home/fabio/TCE/sagres/desktop/Sagres/101092/Importar/");
  }

    public static void renomearExtensaoPraMinusculoDosArquivosDeImportacao(String diretorio) {

        try {
            File f = new File(diretorio);
            File[] listFiles = f.listFiles();
            for (File file : listFiles) {
                String extensao = file.getName().substring(file.getName().lastIndexOf(".") + 1);
                extensao = extensao.toLowerCase();
                String novoNomeArquivo = file.getName().substring(0, file.getName().lastIndexOf(".") + 1);
                //System.out.println(extensao);
                //System.out.println(novoNomeArquivo);
                file.renameTo(new File(diretorio + novoNomeArquivo + extensao));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
