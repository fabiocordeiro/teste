/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.gov.tce.testes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 *
 * @author fabio
 */
public class VerificaNovaVersaoSagres {

   public VerificaNovaVersaoSagres() {
   }

   public boolean comparaVersao(String versaoInstalada) throws MalformedURLException {

        String inputLine;
        String versao = "";
        int pos;

        try {
            URL tceSagres = new URL("http://www/site/servicos-men/mpsagres");
            URLConnection ts;
            ts = tceSagres.openConnection();
            BufferedReader in = new BufferedReader(
                                    new InputStreamReader(
                                    ts.getInputStream()));

            while ((inputLine = in.readLine()) != null) {
                pos = inputLine.indexOf("instalador-do-sagres-2010-versao");
                if (pos>0) {
                  pos+=33;
                  versao = inputLine.substring(pos,pos+3)+"";
                  System.out.println(versao);
                  break;
                }
                //System.out.println(inputLine);
            }
            in.close();
        } catch (IOException ex) {
            //Logger.getLogger(VerificaNovaVersaoSagres.class.getName()).log(Level.SEVERE, null, ex);
        }

        String s=versaoInstalada.replace(".","");
        
        if(versao.equals(s)) {
           return true;
        } else {
          return false;
        }
   }

   public static void main(String[] args) throws MalformedURLException {
      VerificaNovaVersaoSagres versaoNova = new VerificaNovaVersaoSagres();
      System.out.println(versaoNova.comparaVersao("1.6.4"));
   }


}
