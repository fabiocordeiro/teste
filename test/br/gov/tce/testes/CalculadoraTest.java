/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.gov.tce.testes;

import fontes.Calculadora;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author fabio
 */
public class CalculadoraTest {

    public CalculadoraTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSoma() {
        Calculadora c = new Calculadora();
        assertEquals(3, c.soma(1,2));
        
        //fail("The test case is a prototype.");
    }

}